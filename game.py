from random import randint

# Ask human for name input
name = input("Hello! What is your name?")

# Computer will be generating a random number for month and year
for guess_number in range(1, 6):
    month = randint(1, 12)
    year = randint(1990, 2003)

    # Bot asks whether they guess the birthday correctly or not.
    print("Guess", name, ": were you born on ", month, "/", year, "?")

    # Human responds to question
    response = input("Yes or no?")

    # If else statement to end interaction
    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try that again!")
